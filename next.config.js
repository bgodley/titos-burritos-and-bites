/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        hostname: "titos.strapi.bgodley.com"
      }
    ]
  }
}

module.exports = nextConfig
