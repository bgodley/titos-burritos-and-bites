import HeroPage from "@/components/HeroPage"
import FooterSection from "@/sections/FooterSection"
import { Container, Stack, Typography } from "@mui/material"
import Head from "next/head"
import React, { type FC } from "react"
import NavigationBar from "../components/NavigationBar"
import { type GetStaticPathsContext, type GetStaticPropsResult } from "next"
import { strapiGet, urlBuilder } from "@/services/strapi"
import { Box } from "@mui/system"
import BackgroundPattern from "@/components/BackgroundPattern"

interface AboutProps {
  data: {
    attributes: {
      body: string
      image: {
        data?: {
          attributes: {
            url: string
            alternativeText: string
          }
        }
      }
    }
  }
}

const About: FC<AboutProps> = ({ data }) => {
  return (
    <>
      <Head>
        <title>{"About | Tito's Burritos & Bites"}</title>
        <meta name="description" content="Page not found" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <NavigationBar />
        <Box position="relative">
          <BackgroundPattern />
          <Container maxWidth="lg"
            sx={{
              py: 10
            }}
          >
            <HeroPage
              image={urlBuilder(data.attributes.image.data?.attributes.url)}
              alt={data.attributes.image.data?.attributes.alternativeText ?? ""}
              justifyContent={"flex-start"}
            >
              <Stack spacing={2}>
                <Typography variant="h2">
                  About us
                </Typography>
                {data.attributes.body.split("\n").map((line, i) => (
                  <Typography
                    key={i}
                    variant="body1"
                    textAlign="justify"
                  >
                    {line}
                  </Typography>
                ))}
              </Stack>
            </HeroPage>
          </Container>
        </Box>
      </main>
      <footer>
        <FooterSection />
      </footer>
    </>
  )
}

export default About

export async function getStaticProps (ctx: GetStaticPathsContext): Promise<GetStaticPropsResult<AboutProps>> {
  const about = await strapiGet("/api/about-page?populate=image")

  return {
    props: {
      data: about.data
    },
    revalidate: false
  }
}
