import HeroPage from "@/components/HeroPage"
import FooterSection from "@/sections/FooterSection"
import { Button, Container, Stack, Typography } from "@mui/material"
import Head from "next/head"
import React, { type FC } from "react"
import NavigationBar from "../components/NavigationBar"
import titosLanding from "@/imgs/titos-landing.jpg"
import { Box } from "@mui/system"
import Link from "next/link"

const NotFoundPage: FC = () => {
  return (
    <>
      <Head>
        <title>{"Not found | Tito's Burritos & Bites"}</title>
        <meta name="description" content="Page not found" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <NavigationBar />
        <Container maxWidth="lg"
          sx={{
            py: 10
          }}
        >
          <HeroPage image={titosLanding} alt="Page not found">
            <Stack spacing={2}>
              <Typography variant="h2">
                Page not found
              </Typography>
              <Box>
                <Link href="/">
                  <Button variant="contained">
                    Go home
                  </Button>
                </Link>
              </Box>
            </Stack>
          </HeroPage>
        </Container>
      </main>
      <footer>
        <FooterSection />
      </footer>
    </>
  )
}

export default NotFoundPage
