import BackgroundPattern from "@/components/BackgroundPattern"
import CartItem from "@/components/CartItem"
import FooterSection from "@/sections/FooterSection"
import MenuSection, { type MenuItem } from "@/sections/MenuSection"
import { type Cart, getCart, removeOneFromCart, addToCart, removeAllFromCart, type Item } from "@/services/cart"
import { strapiGet } from "@/services/strapi"
import { Box, Button, Container, Grid, Stack, Typography } from "@mui/material"
import { type GetStaticPropsResult } from "next"
import Head from "next/head"
import Link from "next/link"
import React, { useEffect, useState, type FC } from "react"
import NavigationBar from "../components/NavigationBar"

interface OrderPageProps {
  menuItems: MenuItem[]
}

const OrderPage: FC<OrderPageProps> = ({ menuItems }) => {
  const [cart, setCart] = useState<Cart | undefined>(undefined)

  const update = (): void => {
    setCart(getCart())
  }

  useEffect(() => {
    setCart(getCart())
    window.addEventListener("storage", () => {
      update()
    })
  }, [])

  const onRemoveOneClick = (item: Item): void => {
    removeOneFromCart(item.slug)
    update()
  }

  const onAddOneClick = (item: Item): void => {
    addToCart({
      ...item,
      amount: 1
    })
    update()
  }

  const onRemoveAllClick = (item: Item): void => {
    removeAllFromCart(item.slug)
    update()
  }

  let cartView
  if (cart !== undefined && cart.items.length !== 0) {
    cartView = <Box>
      <Grid container spacing={4}>
        <Grid item xs={12} sm={6}>
          <Stack spacing={2}>
            {cart.items.map((item) => (
              <CartItem
                key={item.slug}
                item={item}
                onAddOneClick={onAddOneClick}
                onRemoveAllClick={onRemoveAllClick}
                onRemoveOneClick={onRemoveOneClick}
              />
            ))}
          </Stack>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Stack
            spacing={2}
          >
            <Typography variant="body1">
              Order Total: ${cart.items.reduce((acc, item) => acc + item.price * item.amount, 0)}
            </Typography>
            <Stack
              direction="row"
              spacing={1}
            >
              <Link href="/checkout">
                <Button
                  variant="contained"
                >
                  Check out
                </Button>
              </Link>
              <Link href="/menu">
                <Button
                  variant="outlined"
                >
                  Add more items
                </Button>
              </Link>
            </Stack>
          </Stack>
        </Grid>
      </Grid>
    </Box>
  } else {
    cartView = <>
      <Typography variant="h3">Your order is empty</Typography>
      <Typography variant="body1">Add some items to your order...</Typography>
      <MenuSection items={menuItems} limited={true} showTitle={false} />
    </>
  }

  return (
    <>
      <Head>
        <title>{"Order | Tito's Burritos & Bites"}</title>
        <meta name="description" content="Page not found" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <NavigationBar />
        <Box position="relative">
          <BackgroundPattern />
          <Container maxWidth="lg"
            sx={{
              py: 10
            }}
          >
            <Stack
              spacing={2}
            >
              <Typography
                variant="h2"
                textAlign="center"
                pb={5}
              >
                Your Order
              </Typography>
              {cartView}
            </Stack>
          </Container>
        </Box>
      </main>
      <footer>
        <FooterSection />
      </footer>
    </>
  )
}

export default OrderPage

export async function getStaticProps (): Promise<GetStaticPropsResult<OrderPageProps>> {
  const menuData = await strapiGet("/api/items?populate=image&sort=name&pagination[page]=1&pagination[pageSize]=3&filters[tags][tag][$contains]=Featured")

  return {
    props: {
      menuItems: menuData.data
    },
    revalidate: false
  }
}
