import Head from "next/head"
import React, { type FC } from "react"
import NavigationBar from "../components/NavigationBar"
import { Box, Button, Container, Link, Stack, Typography } from "@mui/material"
import MenuSection, { type MenuItem } from "@/sections/MenuSection"
import { type GetStaticPropsResult } from "next"
import { strapiGet, urlBuilder } from "@/services/strapi"
import FooterSection from "@/sections/FooterSection"
import LandingPage from "@/components/LandingPage"
import BackgroundPattern from "@/components/BackgroundPattern"

interface HomeProps {
  landingPage: {
    attributes: {
      header: string
      body: string
      backgroundImage: {
        data?: {
          attributes: {
            url: string
            alternativeText: string
          }
        }
      }
    }
  }
  menuItems: MenuItem[]
}

const Home: FC<HomeProps> = ({ menuItems, landingPage }) => {
  return (
    <>
      <Head>
        <title>{"Tito's Burritos & Bites"}</title>
        <meta name="description" content="Serving burritos and sandwiches Monday to Friday from 10am to 5pm" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <LandingPage
          image={urlBuilder(landingPage.attributes.backgroundImage.data?.attributes.url)}
          alt={landingPage.attributes.backgroundImage.data?.attributes.alternativeText}
        >
          <NavigationBar />
          <Container
            maxWidth="lg"
            sx={{
              height: "100%"
            }}
          >
            <Stack
              height="100%"
              justifyContent="center"
            >
              <Stack
                maxWidth="max(65%, 300px)"
                spacing={2}
              >
                <Typography
                  variant="h2"
                >
                  {landingPage.attributes.header}
                </Typography>
                <Typography variant="subtitle1" pb={2}>
                  {landingPage.attributes.body}
                </Typography>
                <Box>
                  <Link href="/menu">
                    <Button
                      variant="outlined"
                    >
                      View Menu
                    </Button>
                  </Link>
                </Box>
              </Stack>
            </Stack>
          </Container>
        </LandingPage>
        <Box
          sx={{
            position: "relative"
          }}
        >
          <BackgroundPattern />
          <Container maxWidth="lg" sx={{ py: 10 }}>
            <MenuSection items={menuItems} limited={true} />
          </Container>
        </Box>
      </main>
      <footer>
        <FooterSection />
      </footer>
    </>
  )
}

export default Home

export async function getStaticProps (): Promise<GetStaticPropsResult<HomeProps>> {
  const [landingPage, menuData] = await Promise.all([
    strapiGet("/api/landing-page?populate=backgroundImage"),
    strapiGet("/api/items?populate=image&sort=name&pagination[page]=1&pagination[pageSize]=3&filters[tags][tag][$contains]=Featured")
  ])

  return {
    props: {
      landingPage: landingPage.data,
      menuItems: menuData.data
    },
    revalidate: false
  }
}
