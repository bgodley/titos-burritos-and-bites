import { type MenuItem } from "@/sections/MenuSection"
import { strapiGet, urlBuilder } from "@/services/strapi"
import { Box, Button, Chip, Container, Stack, Typography } from "@mui/material"
import { type GetStaticPathsContext, type GetStaticPathsResult, type GetStaticPropsContext, type GetStaticPropsResult } from "next"
import { type FC } from "react"
import NavigationBar from "@/components/NavigationBar"
import FooterSection from "@/sections/FooterSection"
import { ReactMarkdown } from "react-markdown/lib/react-markdown"
import assert from "assert"
import HeroPage from "@/components/HeroPage"
import Head from "next/head"
import { ArrowLeft } from "@mui/icons-material"
import Link from "next/link"
import { addToCart, type Item } from "@/services/cart"
import { useRouter } from "next/router"

interface MenuItemPageProps {
  item: MenuItem
}

const MenuItemPage: FC<MenuItemPageProps> = ({ item }) => {
  const router = useRouter()

  const addToOrder = (): void => {
    const cartItem: Item = {
      slug: item.attributes.slug,
      price: item.attributes.price,
      name: item.attributes.name,
      amount: 1
    }

    addToCart(cartItem)
    void router.push("/order")
  }

  return (
    <>
      <Head>
        <title>{`${item.attributes.name} | Tito's Burritos & Bites`}</title>
        <meta name="description" content={item.attributes.description} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <NavigationBar />
        <Container maxWidth="lg" sx={{ py: 10 }}>
          <Link href="/menu">
            <Button
              startIcon={<ArrowLeft />}
              variant="text"
              sx={{
                mb: 4
              }}
            >
              Back to Menu
            </Button>
          </Link>
          <HeroPage
            image={urlBuilder(item.attributes.image?.data.attributes.url)}
            alt={item.attributes.image?.data.attributes.alternativeText ?? ""}
            justifyContent="flex-start"
          >
            <Stack spacing={2}>
              {item.attributes.tag !== undefined && <Box>
                <Chip variant="filled" color="primary" label={item.attributes.tag?.data.attributes.tag ?? ""} />
              </Box>}
              <Typography variant="h2">
                {item.attributes.name}
              </Typography>
              <Typography variant="subtitle1">
                ${item.attributes.price}
              </Typography>
              <ReactMarkdown>
                {item.attributes.description}
              </ReactMarkdown>
              <Box>
                <Button
                  variant="contained"
                  onClick={() => { addToOrder() }}
                >
                  Add to order
                </Button>
              </Box>
            </Stack>
          </HeroPage>
        </Container>
      </main>
      <footer>
        <FooterSection />
      </footer>
    </>
  )
}
export default MenuItemPage

export async function getStaticProps (ctx: GetStaticPropsContext): Promise<GetStaticPropsResult<MenuItemPageProps>> {
  const { slug } = ctx.params as { slug: string }
  const data = await strapiGet(`/api/items?filters[slug][$eq]=${slug}&populate=image&populate=tag`)

  assert(data.data.length === 1, new Error("Invalid item slug"))

  return {
    props: {
      item: data.data[0]
    },
    revalidate: false
  }
}

export async function getStaticPaths (_ctx: GetStaticPathsContext): Promise<GetStaticPathsResult> {
  const paths = await strapiGet("/api/items?fields[0]=slug")

  return {
    paths: paths.data.map((path: MenuItem) => ({
      params: {
        slug: path.attributes.slug
      }
    })),
    fallback: false
  }
}
