import BackgroundPattern from "@/components/BackgroundPattern"
import NavigationBar from "@/components/NavigationBar"
import FooterSection from "@/sections/FooterSection"
import MenuSection, { type MenuItem } from "@/sections/MenuSection"
import { strapiGet } from "@/services/strapi"
import { Box, Container } from "@mui/material"
import { type GetStaticPropsContext, type GetStaticPropsResult } from "next"
import Head from "next/head"
import { type FC } from "react"

interface MenuPageProps {
  items: MenuItem[]
}

const MenuPage: FC<MenuPageProps> = ({ items }) => {
  return (
    <>
      <Head>
        <title>{"Menu | Tito's Burritos & Bites"}</title>
        <meta name="description" content="Serving burritos, sandwiches and refreshments" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <NavigationBar />
        <Box position="relative">
          <BackgroundPattern />
          <Container maxWidth="lg" sx={{ py: 10 }}>
              <MenuSection items={items} limited={false}/>
          </Container>
        </Box>
      </main>
      <footer>
        <FooterSection />
      </footer>
    </>
  )
}
export default MenuPage

export async function getStaticProps (ctx: GetStaticPropsContext): Promise<GetStaticPropsResult<MenuPageProps>> {
  const data = await strapiGet("/api/items?populate=image&sort=name")

  return {
    props: {
      items: data.data
    },
    revalidate: false
  }
}
