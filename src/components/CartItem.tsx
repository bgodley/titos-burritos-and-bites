import { type Item } from "@/services/cart"
import { Remove, Add, Delete } from "@mui/icons-material"
import { Paper, Stack, Typography, IconButton } from "@mui/material"
import { type FC } from "react"

interface CartItemProps {
  item: Item
  onRemoveOneClick: (item: Item) => void
  onAddOneClick: (item: Item) => void
  onRemoveAllClick: (item: Item) => void
}

const CartItem: FC<CartItemProps> = ({ item, onRemoveAllClick, onAddOneClick, onRemoveOneClick }) => {
  return (
    <Paper
      key={item.slug}
      sx={{
        p: 2
      }}
    >
      <Stack
        direction="row"
        spacing={1}
      >
        <Typography
          variant="body1"
          fontWeight={600}
          flex={1}
        >
          {item.name}
        </Typography>
        <Typography
          variant="body1"
          textAlign="right"
          flex={1}
        >
          ${item.price * item.amount}
        </Typography>
      </Stack>
      <Stack
        mt={2}
        direction="row"
        justifyContent="space-between"
      >
        <Stack
          direction="row"
          spacing={1}
        >
          <IconButton
            color="inherit"
            onClick={() => { onRemoveOneClick(item) }}
          >
            <Remove />
          </IconButton>
          <Typography>
            {item.amount}
          </Typography>
          <IconButton
            color="inherit"
            onClick={() => { onAddOneClick(item) }}
          >
            <Add />
          </IconButton>
        </Stack>
        <IconButton
          color="inherit"
          onClick={() => { onRemoveAllClick(item) }}
        >
          <Delete />
        </IconButton>
      </Stack>
    </Paper>
  )
}
export default CartItem
