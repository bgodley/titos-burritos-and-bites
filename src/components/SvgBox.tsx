import { Box, type Theme } from "@mui/material"
import { type BoxProps } from "@mui/system"
import { type PropsWithChildren, type FC } from "react"

interface SvgBoxProps extends BoxProps {
  src: string
  backgroundSize?: string | number
  backgroundColor?: string | ((theme: Theme) => string)
}

const SvgBox: FC<PropsWithChildren<SvgBoxProps>> = ({ src, backgroundSize = "50px", backgroundColor = "#fff", sx, ...props }) => {
  return (
    <Box
      sx={{
        flex: 1,
        backgroundImage: `url(${src})`,
        backgroundRepeat: "repeat",
        backgroundSize,
        backgroundColor,
        ...sx
      }}
      {...props}
    />
  )
}
export default SvgBox
