import { Box } from "@mui/material"
import { type FC } from "react"
import SvgBox from "./SvgBox"
import hideout from "@/imgs/hideout.svg"

const BackgroundPattern: FC = () => {
  return (
    <SvgBox
      backgroundSize="6%"
      backgroundColor="transparent"
      sx={{
        position: "absolute",
        bottom: 0,
        right: 0,
        width: "33%",
        height: "80%",
        zIndex: -1
      }}
      src={hideout.src}
    >
      <Box
        sx={{
          position: "absolute",
          top: 0,
          left: 0,
          width: "100%",
          height: "100%",
          background: theme => `radial-gradient(100% 100% at 100% 100%, ${theme.palette.background.default}aa 0%, ${theme.palette.background.default} 100%)`,
          zIndex: 1
        }}
      />
    </SvgBox>
  )
}
export default BackgroundPattern
