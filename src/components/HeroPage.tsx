import { Box, Grid, Hidden, type StackProps, type GridProps } from "@mui/material"
import React, { type FC, type PropsWithChildren } from "react"
import Image, { type ImageProps, type StaticImageData } from "next/image"

interface HeroPageProps extends GridProps {
  image: StaticImageData | string
  loader?: ImageProps["loader"]
  alt: string
  reverse?: boolean
  form?: boolean
  imageHiddenMobile?: boolean
  justifyContent?: StackProps["justifyContent"]
}

const HeroPage: FC<PropsWithChildren<HeroPageProps>> = ({ image, alt, loader, justifyContent = "center", reverse = false, form = false, imageHiddenMobile = false, children, ...props }) => {
  const heroPageImage = (
    <Box
      sx={{
        position: "relative",
        maxWidth: "100%",
        height: "100%",
        aspectRatio: "1 / 1"
      }}
    >
      <Image
        src={image}
        loader={loader}
        alt={alt}
        fill
        style={{
          objectFit: "cover"
        }}
      />
    </Box>
  )

  const content = <Box
    sx={{
      width: "100%",
      height: "100%",
      display: "flex",
      flexDirection: "column",
      justifyContent
    }}
  >
    {children}
  </Box>

  if (!reverse) {
    return (<Box>
      <Grid container {...props} spacing={4}>
        <Hidden mdDown={imageHiddenMobile}>
          <Grid item xs={12} md={6}>
            {heroPageImage}
          </Grid>
        </Hidden>
        <Grid item xs={12} md={6}>
          {content}
        </Grid>
      </Grid>
    </Box>)
  } else {
    return (<Box>
      <Grid container {...props}>
        <Grid item xs={12} md={6}>
          {content}
        </Grid>
        <Hidden mdDown={imageHiddenMobile}>
          <Grid item xs={12} md={6}>
            {heroPageImage}
          </Grid>
        </Hidden>
      </Grid>
    </Box>)
  }
}
export default HeroPage
