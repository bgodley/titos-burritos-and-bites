import React, { useState } from "react"
import { type FC } from "react"
import { Box, Button, Container, Drawer, Hidden, IconButton, Stack, Typography } from "@mui/material"
import MenuIcon from "@mui/icons-material/Menu"
import Link from "next/link"

const appName = "Tito's Burritos & Bites"
const navLinks = {
  Home: "/",
  Menu: "/menu",
  About: "/about"
}

const NavigationBarMobile: FC = () => {
  const [open, setOpen] = useState<boolean>(false)

  return (<>
    <Container
      maxWidth="lg"
    >
      <Stack
        width="100%"
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        py={2}
      >
        <Link
          href="/"
        >
          <Typography
            variant="h1"
          >
            {appName}
          </Typography>
        </Link>
        <IconButton
          color="inherit"
          onClick={() => { setOpen(true) }}
        >
          <MenuIcon />
        </IconButton>
      </Stack>
    </Container>

    <Drawer
      onClick={() => { setOpen(false) }}
      onClose={() => { setOpen(false) }}
      anchor="top"
      open={open}
    >
      <Stack
        spacing={3}
        p={2}
        sx={{
          backgroundColor: theme => theme.palette.background.default
        }}
      >
        {
          Object.entries(navLinks).map(([text, link]) => (
            <Link key={text} href={link}>
              <Button>
                {text}
              </Button>
            </Link>
          ))
        }
      </Stack>
    </Drawer>
  </>)
}

const NavigationBarDesktop: FC = () => {
  return (
    <Container
      sx={{
        zIndex: 10
      }}
      maxWidth="xl"
    >
      <Stack
        width="100%"
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        py={2}
      >
        <Link
          href="/"
        >
          <Typography
            variant="h1"
          >
            {appName}
          </Typography>
        </Link>
        <Stack
          direction="row"
          spacing={3}
          alignItems="baseline"
        >
          {
            Object.entries(navLinks).map(([text, link]) => (
              <Link key={text} href={link}>
                <Button
                  color="secondary"
                >
                  {text}
                </Button>
              </Link>
            ))
          }
          <Link href="/order">
            <Button
              variant="contained"
              color="primary"
            >
              Order Now
            </Button>
          </Link>
        </Stack>
      </Stack>
    </Container>
  )
}

const NavigationBar: FC = () => {
  return <Box>
    <Hidden mdDown>
      <NavigationBarDesktop />
    </Hidden>
    <Hidden mdUp>
      <NavigationBarMobile />
    </Hidden>
  </Box>
}

export default NavigationBar
