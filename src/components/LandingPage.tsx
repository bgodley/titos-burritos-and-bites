import { Box } from "@mui/material"
import { type FC, type PropsWithChildren } from "react"
import Image from "next/image"

interface LandingPageProps {
  image?: string
  alt?: string
}

const LandingPage: FC<PropsWithChildren<LandingPageProps>> = ({ image, alt, children }) => {
  return (
    <Box
      sx={{
        position: "relative",
        width: "100%",
        aspectRatio: "1 / 1",
        minHeight: "500px",
        maxHeight: "100lvh",
        overflow: "hidden"
      }}
    >
      <Image
        fill
        style={{ objectFit: "cover", filter: "brightness(30%) blur(0px) saturate(150%)" }}
        src={image ?? ""}
        alt={alt ?? ""}
      />
      <Box
        sx={{
          position: "absolute",
          width: "100%",
          height: "100%",
          display: "flex",
          flexDirection: "column"
        }}
      >
        {children}
      </Box>
    </Box>
  )
}
export default LandingPage
