import { urlBuilder } from "@/services/strapi"
import { ArrowRight } from "@mui/icons-material"
import { Box, Button, Grid, Typography } from "@mui/material"
import Image from "next/image"
import Link from "next/link"
import React from "react"
import { type FC } from "react"

export interface MenuItem {
  attributes: {
    slug: string
    name: string
    price: number
    description: string
    image?: {
      data: {
        id: number
        attributes: {
          name: string
          alternativeText: string
          caption: string
          width: number
          height: number
          url: string
          formats: {
            thumbnail: {
              url: string
            }
          }
        }
      }
    }
    tag?: {
      data: {
        id: number
        attributes: {
          tag: string
          description: string
        }
      }
    }
  }
}

export interface MenuSectionProps {
  items: MenuItem[]
  limited?: boolean
  showTitle?: boolean
}

const MenuSection: FC<MenuSectionProps> = ({ items, limited = false, showTitle = true }) => {
  if (limited) {
    items = items.slice(0, 3)
  }

  return (
    <Box>
      {showTitle && <Typography
        variant="h2"
        textAlign="center"
        mb={10}
      >
        Our Menu
      </Typography>}

      <Grid container
        spacing={2}
      >
        {
          items.map((item) => (
            <Grid item
              key={item.attributes.slug}
              xs={4}
              md={!limited && 3}
            >
              <Link href={`/menu/${item.attributes.slug}`} >
                <Box>
                  <Box
                    sx={{
                      width: "100%",
                      aspectRatio: "1",
                      position: "relative"
                    }}
                  >
                    <Image
                      fill
                      style={{
                        objectFit: "cover"
                      }}
                      src={urlBuilder(item.attributes.image?.data.attributes.url)}
                      alt={item.attributes.image?.data?.attributes.alternativeText ?? ""}
                    />
                    <Box
                      sx={{
                        bgcolor: theme => theme.palette.primary.main,
                        position: "absolute",
                        width: "40%",
                        height: "20%",
                        bottom: 0,
                        right: 0,
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Typography
                        color="black"
                        variant="h4"
                      >
                        ${item.attributes.price}
                      </Typography>
                    </Box>
                  </Box>
                  <Typography variant="h4" mt={1}>{item.attributes.name}</Typography>
                </Box>
              </Link>
            </Grid>
          ))
        }
      </Grid>
      {limited && (
        <Box
          sx={{
            mt: 4,
            display: "flex",
            justifyContent: "center"
          }}
        >
          <Link href="/menu">
            <Button
              variant="contained"
              endIcon={<ArrowRight />}
            >
              View full menu
            </Button>
          </Link>
        </Box>
      )}
    </Box>
  )
}
export default MenuSection
