import { Box, Button, Container, Grid, Stack, Typography } from "@mui/material"
import Image from "next/image"
import Link from "next/link"
import { type FC } from "react"
import nightBridge from "../imgs/night-bridge.jpg"

const FooterSection: FC = () => {
  return (<>
      <Box
        sx={{
          position: "relative"
        }}
      >
        <Image
          fill
          src={nightBridge}
          alt="Tito's Footer"
          style={{
            zIndex: -1,
            objectFit: "cover",
            filter: "brightness(30%)"
          }}
        />

        <Container
          maxWidth="lg"
          sx={{
            py: 4
          }}
        >
          <Grid container
            rowSpacing={4}
          >
            <Grid item xs={6} sm={4}>
              <Typography variant="h1">Tito&apos;s Burritos & Bites</Typography>
            </Grid>
            <Grid item xs={6} sm={4}>
              <Stack>
                <Link href="/">
                  <Button>
                    Home
                  </Button>
                </Link>
                <Link href="/menu">
                  <Button>
                    Menu
                  </Button>
                </Link>
                <Link href="/order">
                  <Button sx={{ textAlign: "left" }}>
                    Order Online
                  </Button>
                </Link>
                <Link href="/about">
                  <Button>
                    About
                  </Button>
                </Link>
              </Stack>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Stack
                height="100%"
                justifyContent="flex-end"
              >
                <Typography variant="button" textAlign="right">
                  Website by <Link href="https://bgodley.com">Braden Godley</Link>
                </Typography>
              </Stack>
            </Grid>
          </Grid>
        </Container>
      </Box>
  </>)
}
export default FooterSection
