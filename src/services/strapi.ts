import axios from "axios"
import axiosRetry from "axios-retry"

const STRAPI_HOST = process.env.NEXT_PUBLIC_STRAPI_HOST
const STRAPI_API_TOKEN = process.env.STRAPI_API_TOKEN

axios.defaults.headers.common.Authorization = `Bearer ${STRAPI_API_TOKEN as string}`

axiosRetry(axios, {
  retryDelay: axiosRetry.exponentialDelay,
  retries: 3
})

export function urlBuilder (path: string | undefined): string {
  if (path === undefined) {
    return ""
  }

  const url = `https://${STRAPI_HOST ?? "localhost:1337"}${path}`

  return url
}

export async function strapiGet (path: string): Promise<any> {
  const url = urlBuilder(path)

  const res = await axios.get(url)

  return res.data
}
