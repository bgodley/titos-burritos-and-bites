export const CART_KEY = "savedCart"
const CART_EXPIRATION = 1000 * 60 * 60 * 24 * 7 // 7 days = 1000ms * 60s * 60m * 24hr * 7d

export interface Cart {
  dateModified: Date
  items: Item[]
}

export interface Item {
  slug: string
  name: string
  amount: number
  price: number
}

export function getCart (): Cart {
  if (typeof window !== "undefined") {
    const savedCart = window.localStorage.getItem(CART_KEY)
    if (savedCart !== null) {
      const cart = JSON.parse(savedCart) as Cart
      cart.dateModified = new Date(cart.dateModified)
      if (cart.dateModified !== undefined && (cart.dateModified.getTime() + CART_EXPIRATION) > (new Date()).getTime()) {
        return cart
      }
    }
  }

  return {
    dateModified: new Date(),
    items: []
  }
}

export function saveCart (cart: Cart): void {
  if (typeof window === "undefined") return

  cart.dateModified = new Date()
  const json = JSON.stringify(cart)
  window.localStorage.setItem(CART_KEY, json)
}

export function addToCart (newItem: Item): void {
  const cart = getCart()
  const existingItem = cart.items.find((item) => item.slug === newItem.slug)

  if (existingItem !== undefined) {
    existingItem.amount += newItem.amount
  } else {
    cart.items.push(newItem)
  }
  saveCart(cart)
}

export function removeOneFromCart (slug: string): void {
  const cart = getCart()
  // Reduce the item amount by 1
  cart.items = cart.items.map((item) => {
    if (item.slug === slug) {
      item.amount -= 1
    }
    return item
  })
  // Delete items that have no amount
  cart.items = cart.items.filter((item) => item.amount > 0)
  saveCart(cart)
}

export function removeAllFromCart (slug: string): void {
  const cart = getCart()
  // Remove all items from the cart
  cart.items = cart.items.filter((item) => item.slug !== slug)
  saveCart(cart)
}

export function clearCart (): void {
  const cart = getCart()
  // Clear all items from the cart
  cart.items = []

  saveCart(cart)
}
