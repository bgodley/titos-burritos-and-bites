import { createTheme, responsiveFontSizes, type ThemeOptions } from "@mui/material/styles"
import { deepmerge } from "@mui/utils"
import { Bebas_Neue, Lato } from "@next/font/google"

const hubballi = Bebas_Neue({ display: "swap", subsets: ["latin"], weight: "400" })
const ptSans = Lato({ display: "swap", subsets: ["latin"], weight: ["400"] })

const themeBase: ThemeOptions = {
  typography: {
    fontFamily: [
      ptSans.style.fontFamily,
      "-apple-system",
      "BlinkMacSystemFont",
      "\"Segoe UI\"",
      "\"Roboto\"",
      "\"Helvetica\"",
      "\"Arial\"",
      "sans-serif",
      "\"Apple Color Emoji\"",
      "\"Segoe UI Emoji\"",
      "\"Segoe UI Symbol\""
    ].join(","),
    h1: {
      fontFamily: hubballi.style.fontFamily,
      fontSize: 40,
      letterSpacing: 2,
      textTransform: "uppercase"
    },
    h2: {
      fontFamily: hubballi.style.fontFamily,
      fontSize: 80,
      lineHeight: 1,
      letterSpacing: 2,
      textTransform: "uppercase"
    },
    h4: {
      fontFamily: hubballi.style.fontFamily,
      fontSize: 40,
      lineHeight: 1,
      letterSpacing: 2,
      textTransform: "uppercase"
    },
    body1: {
      fontSize: 24
    },
    subtitle1: {
      fontSize: 32
    },
    button: {
      fontFamily: hubballi.style.fontFamily,
      letterSpacing: 2,
      fontSize: 24,
      textTransform: "uppercase"
    }
  },
  // shadows: Array(25).fill("none"),
  shape: {
    borderRadius: 0
  },
  components: {
    MuiButton: {
      defaultProps: {
        disableRipple: true
      },
      styleOverrides: {
        root: ({ ownerState }) => {
          if (ownerState.variant === "text") {
            return {
              backgroundColor: "transparent",
              ":hover": {
                backgroundColor: "transparent"
              }
            }
          } else {
            return {}
          }
        }
      }
    },
    MuiLink: {
      styleOverrides: {
        root: {
          color: "inherit",
          textDecoration: "none"
        }
      }
    }
  }
}

const palette: ThemeOptions = {
  palette: {
    mode: "light",
    primary: {
      main: "hsl(50, 90%, 60%)",
      contrastText: "#0F0F0F"
    },
    secondary: {
      main: "#fff",
      contrastText: "#000"
    },
    text: {
      primary: "#FFF",
      secondary: "#FFF"
    },
    common: {
      white: "#FFF",
      black: "#020202"
    },
    background: {
      paper: "#222",
      default: "#0F0F0F"
    }
  }
}

const theme = responsiveFontSizes(createTheme(deepmerge(themeBase, palette)))

export { theme }
